var angularHopepage = require('../../pageObjects/angularHomePage.po');

beforeAll(async function(){
    await angularHopepage.get();
});

describe('angularjs homepage', function(){
    it('should greet the named user', async function(){
        await angularHopepage.setName('Shaila');

        expect(await angularHopepage.getGreetingText()).toEqual('Hello Shaila!');

    });
});


describe('Enter todo feature', function(){
    it('Should add a new todo in the todo list', async function(){

        await angularHopepage.setToDoText('Automated todo 1');
        await angularHopepage.clicktodoAddButton();

        expect(await angularHopepage.getTotalToDos()).toEqual(3);
        expect(await angularHopepage.getNewlyEnteredTodoText()).toEqual('Automated todo 1');
    });
});


describe('Cross newly created todo from todo list', function(){
    it('Should reduce 1 todo from todo list', async function(){

        expect(await angularHopepage.crossNewlyCreatedTodo()).toEqual(2);
    });
});


describe('Verify homepage header and title', function(){
    it('Should display header and title', async function(){

        expect(await angularHopepage.getHomePageHeader()).toEqual('Why AngularJS?');
        expect(browser.getTitle()).toEqual('AngularJS — Superheroic JavaScript MVW Framework');
        console.log('test');
    });
});
