var superCalculator = require('../../pageObjects/superCalculator.po');

// beforeAll(async function(){
//     await superCalculator.get();
// });

describe('Calculator addition operation test', function(){
    beforeEach(async function(){
        await superCalculator.get();
    });

    it('Get site title', async function(){
        expect(await browser.getTitle()).toEqual('Super Calculator');
    });

    it('Addition operation test for 5 and 2', async function(){
        await superCalculator.setfirstInput(5);
        await superCalculator.setsecondInput(2);
        // await superCalculator.selectsubtractionOperator();
        superCalculator.clickgoButton();

        expect(superCalculator.getResult()).toEqual('7');
    })
    
});

describe('Verify history of claculation', function(){
    beforeEach(async function(){
        await superCalculator.get();
    });


    it('Should have 2 histories', async function(){
        superCalculator.add(3,5);
        expect(await superCalculator.getHistory()).toEqual(1);

        superCalculator.add(100,200);
        expect(await superCalculator.getHistory()).toEqual(2);
    });
});

describe('Calculator subtractior operation test', function(){
    it('Should select subtractor operator', async function(){
        expect(await superCalculator.selectsubtractionOperator()).toEqual('-');

        superCalculator.subtraction(100, 50);
        expect(await superCalculator.getResult()).toEqual('50');
    });
});

// select option:
// element(by.cssContainingText('option', 'BeaverBox Testing')).click();
// expect(element(by.xxxxxx('xxxxxxx')).$('option:checked').getText()).toEqual('xxxxxx')