
var AngularHomepagepo = function(){
    var nameInput = element(by.model('yourName'));
    var greeting = element(by.binding('yourName'));
    var todoListTextInput = element(by.model('todoList.todoText'));
    var todoAddButton =  element(by.xpath('/html/body/div[2]/div[3]/div[2]/div[2]/div/form/input[2]'));
    var totalToDos = element.all(by.repeater('todo in todoList.todos'));
    var completedTodos = element.all(by.css('.done-true'));
    var homePageHeader = element(by.css('div > div:nth-child(1) > h3'));
        
    this.get = async function(){
        await browser.get('https://angularjs.org/');
    };

    this.setName = async function(name){
        await nameInput.sendKeys(name);
    };

    this.getGreetingText = function(){
        return greeting.getText();
    };

    this.getGreeting = function(){
        return greeting;
    };

    this.setToDoText = async function(todoText){
        await todoListTextInput.sendKeys(todoText);
    };

    this.clicktodoAddButton = async function(){
        await todoAddButton.click();
    };

    this.getTotalToDos =  function(){
        return totalToDos.count();
    };

    this.getNewlyEnteredTodoText = function(){
        return totalToDos.get(2).getText();
    }

    this.crossNewlyCreatedTodo = async function(){
        await totalToDos.get(2).element(by.model('todo.done')).click();
        return completedTodos.count(2);
    }

    this.getHomePageHeader = function(){
        return homePageHeader.getText();
    }


};

module.exports = new AngularHomepagepo();