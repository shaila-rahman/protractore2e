var SuparCalculatorpo = function(){
    var firstInput = element(by.model('first'));
    var secondInput = element(by.model('second'));
    var goButton = element(by.id('gobutton'));
    var result = element(by.className('ng-binding'));
    var history = element.all(by.repeater('result in memory'));
    var operatorDropDown = element.all(by.options('value for (key, value) in operators'));


    this.get = async function(){
        await browser.get('http://juliemr.github.io/protractor-demo/');
    };

    this.setfirstInput = async function(number1){
        await firstInput.sendKeys(number1);
    };
    
    this.setsecondInput = async function(number2){
        await secondInput.sendKeys(number2);
    };

    this.selectsubtractionOperator = function(){
         operatorDropDown.click();
        var subtractor = operatorDropDown.get(4);
        subtractor.click();
        return subtractor.getText();
    };

    this.clickgoButton = function(){
        goButton.click();
    };

    this.getResult = function(){
        return result.getText();
    };

    this.add = function(number1, number2){
        firstInput.sendKeys(number1);
        secondInput.sendKeys(number2);
        goButton.click();
    };

    this.subtraction = function(number1, number2){
        firstInput.sendKeys(number1);
        secondInput.sendKeys(number2);
        goButton.click();
    }
        

    this.getHistory = function(){
        return history.count();
    };
};

module.exports = new SuparCalculatorpo();