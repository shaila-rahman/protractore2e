let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
    
    seleniumAddress : 'http://localhost:4444/wd/hub',
    // specs: ['spec.js'],
    // specs: ['./specs/*spec.js'],

    multiCapabilities:[{
        browserName: 'firefox'
      },{
        browserName: 'chrome'
      }],

     
    suites:{
      homepage: './specs/angularHomepage/*spec.js',
      calculator: './specs/suparCalculator/*spec.js'
    },

    resultJsonOutputFile:'./testLog/testLog.json',

    onPrepare: function(){
        jasmine.getEnv().addReporter(new SpecReporter({
          displayFailuresSummary: true,
          displayFailuredSpec: true,
          displaySuiteNumber: true,
          displaySpecDuration: true
        }));
    }
    
};

